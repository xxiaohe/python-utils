import re
import os

"""
描述：读取文件内容为"|资产处置损益|下级行存放备付金|"内容，并返回set集合
"""


def readFormatFile1(path):
    pageStrs = set();
    with open(path, 'r', encoding='utf-8') as file:
        data = file.read()
        words = re.split("｜\s*", data)
        for line in words:
            words1 = re.split("\\|\s*", line)
            for line1 in words1:
                line1 = line1.replace('\n', "").replace('（', "(").replace('）', ")")
                pageStrs.add(line1.strip())
    return pageStrs


"""
描述：写文件，格式为"|资产处置损益|下级行存放备付金|"
"""


def writeFormatFile1(path, list):
    with open(path, 'w', encoding='utf-8') as file:
        for item in list:
            file.write(item + "|")


"""
描述：写文件
"""


def writeOrignFile(path, list):
    with open(path, 'w', encoding='utf-8') as file:
        for item in list:
            file.write(item+'\n')

"""
描述：写文件
"""


def writeOrignFile2(path, type, list):
    with open(path, type, encoding='utf-8') as file:
        for item in list:
            file.write(str(item)+'\n')


"""
描述：遍历指定前缀文件夹，获得文件夹下所有后缀文件路径列表
"""


def traversalFolder(folder_path, pre, end):
    targetFiles = []
    for root, dirs, files in os.walk(folder_path):
        # 遍历当前文件夹下的所有文件
        for file_name in files:
            if file_name.endswith(end):
                # file_path = os.path.join(root, file_name)
                file_path = root.replace("\\\\", "\\").replace("\\", "\\\\") + "\\\\" + file_name
                targetFiles.append(file_path)
        for dir in dirs:
            if dir.startswith(pre):
                dir_path = os.path.join(root, dir)
                traversalFolder(dir_path, pre, end)
    return targetFiles


"""
描述：Properties文件
"""


def readPropertiesFile(path):
    properties = {}
    with open(path, 'r', encoding='utf-8') as file:
        for line in file.readlines():
            if re.match(r'^\s*#', line):
                continue
            result = re.match(r'^\s*([^=]+)\s*=\s*(.*)\s*$', line)
            if result:
                properties[result.group(1)] = result.group(2)
    return properties


if __name__ == "__main__":
    # path = r"D:\\working\\FY21_SmartGL\\frontend-dev2\\SourceCode\\backend-dev\\bicenterfin\\WebRoot\\WEB-INF\\classes\\lang\\message_en_US.properties"
    path = "D:\\tmp\\files\\lang\\message_zh_CN.properties"
    print(readPropertiesFile(path).values())
