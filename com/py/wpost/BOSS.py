import pandas as pd
from scrapy import signals
from selenium import webdriver
from scrapy.http.response.html import HtmlResponse
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
import re
import json
import time
import ast
import pandas as pd
import com.py.utils.FileUtils as fileUtils

regex0 = '[\u4e00-\u9fa5|\-|\/]'

path = r"D:\temp\岗位\BossJava职位招聘.txt"
def getPost():
    chrome_driver_path = 'D:\\WorkingSpace\\pythonSpace\\bicenter_reptile\\demo\\chromedriver.exe'
    # 创建-个 Chrome 浏览器实例，并传入 ChromeDriver 的路径
    service = Service(executable_path=chrome_driver_path)
    # 启动Chrome浏览器
    web = webdriver.Chrome(service=service)
    web.maximize_window()
    url = r"https://www.zhipin.com/chengdu/"
    targets = []
    web.get(url)
    time.sleep(2)
    web.find_element(By.XPATH, '//*[@id="wrap"]/div[3]/div/div[1]/div[1]/form/div[2]/p/input').send_keys("java开发")
    web.find_element(By.XPATH, '//*[@id="wrap"]/div[3]/div/div[1]/div[1]/form/button').click()
    time.sleep(4)
    web.find_element(By.XPATH, '/html/body/div[9]/div[2]/div[1]/span/i').click()
    time.sleep(1)
    liList = web.find_elements(By.XPATH, '//*[@id="wrap"]/div[2]/div[2]/div/div[1]/div[2]/ul/li')
    for liTag in liList:
        href = liTag.find_element(By.TAG_NAME, 'a')
        print(href)
        # info = {
        #     'name': tdList[2].text + " " + tdList[3].text,
        #     'vehicleModel': tdList[4].text,
        #     'trademark': tdList[2].text,
        #     'referencePrice': 0,
        #     'size': 0,
        #     'fuelTank': 0,
        #     'maxSpeed': 0,
        #     'fuelConsumption': tdList[5].text,
        #     'sample': tdList[6].text
        # }
        # print(info)
        # fileUtils.writeOrignFile2(path, 'a', [info])
        # targets.append(info)
    time.sleep(10)
    web.close()
    return targets


def txtToExcel():
    # 打开文件
    # 创建两个DataFrame用于生成不同的sheet页
    C_name = []
    C_vehicleModel = []
    C_trademark = []
    C_referencePrice = []
    C_size = []
    C_fuelTank = []
    C_maxSpeed = []
    C_fuelConsumption = []
    C_sample = []
    with open(path, 'r', encoding='utf-8') as file:
        # 按行读取
        for line in file:
            print(line)
            tar = ast.literal_eval(line.strip('\n'))
            C_name.append(tar['name'])
            C_vehicleModel.append(tar['vehicleModel'])
            C_trademark.append(tar['trademark'])
            C_referencePrice.append(tar['referencePrice'])
            C_size.append(tar['size'])
            C_fuelTank.append(tar['fuelTank'])
            C_maxSpeed.append(float(tar['maxSpeed']))
            C_fuelConsumption.append(
                0 if len(re.findall(regex0, tar['fuelConsumption'])) > 0 else float(tar['fuelConsumption']))
            C_sample.append(float(tar['sample']))
    df1 = pd.DataFrame({'名称': C_name, '类型': C_vehicleModel, '品牌': C_trademark, '参考价格': C_referencePrice, '常长宽高': C_size,
                        '邮箱大小': C_fuelTank, '最高速度': C_maxSpeed, '百公里耗油': C_fuelConsumption, '样本数': C_sample})
    with pd.ExcelWriter(r'D:\temp\摩托车信息\110CC小熊油耗摩托车排行%s.xlsx' % int(time.time())) as writer:
        df1.to_excel(writer, sheet_name='110CC小熊油耗排行')


if __name__ == "__main__":
    getPost()
    # txtToExcel()
