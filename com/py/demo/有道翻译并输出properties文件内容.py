import sys

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
import time
import re

chrome_driver_path = 'D:\\WorkingSpace\\pythonSpace\\bicenter_reptile\\demo\\chromedriver.exe'
# 创建-个 Chrome 浏览器实例，并传入 ChromeDriver 的路径
service = Service(executable_path=chrome_driver_path)
web = webdriver.Chrome(service=service)
web.get("https://fanyi.youdao.com/#/")

time.sleep(2)
web.find_element(By.XPATH, '/html/body/div[1]/div[1]/div/div[2]/div[2]/div/div[1]/div/div[1]/div[1]/div[1]').click()

sqls = []
zhLabel = []
enLabel = []
with open('D:\\temp\\有道翻译原词.txt', 'r', encoding='utf-8') as file:
    data = file.read()
    words = re.split("｜\s*", data)
    keyStart = "lg_rsp_prompt_"
    num = 3
    for line in words:
        targets = re.split("\\|\s*", line)
        for target in targets:
            target = target.strip(",").strip("'")
            try:
                web.find_element(By.XPATH, '//*[@id="js_fanyi_input"]').clear()
                web.find_element(By.XPATH, '//*[@id="js_fanyi_input"]').send_keys(target.strip())
                time.sleep(2)
                value = web.find_element(By.XPATH, '//*[@id="js_fanyi_output_resultOutput"]/p/span').text
                num += 1
                indexS = str(num)
                if num < 10:
                    indexS = "0" + indexS
                strZh = "%s%s=%s" % (keyStart, indexS, target.strip())
                strEn = "%s%s=%s" % (keyStart, indexS, value)
                zhLabel.append(strZh)
                enLabel.append(strEn)
                print("t4==1==>%s,%s,%s" % (target.strip(), strZh, strEn))
            except Exception as e:
                print(e)
web.close()

with open("D:\\temp\\有道翻译后词ZH.txt", "w", encoding='utf-8') as file:
    for element in zhLabel:
        file.write(str(element) + "\n")
with open("D:\\temp\\有道翻译后词EN.txt", "w", encoding='utf-8') as file:
    for element in enLabel:
        file.write(str(element) + "\n")
sys.exit()
