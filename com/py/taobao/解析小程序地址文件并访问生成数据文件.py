import json
import requests

def getUrls(path):
    data = {}
    with open(path, 'r', encoding='utf-8') as file:
        data = json.loads(file.read())
    return data


def reqBody(url, filePath):
    body = requests.get(url)
    content = body.text
    if content and filePath:
        with open(filePath, 'w', encoding='utf-8') as file:
            file.write(content)


if __name__ == "__main__":
    urls = getUrls(r'D:\WorkingSpace\wxSpace\main\wx-applet\doc\小程序数据接口信息.txt')
    reqBody(urls['获取首页数据'],
            r"D:\WorkingSpace\wxSpace\main\wx-applet\applet-uis\yiwub-ui\static\apiJson\index_main_api.json")
    reqBody(urls['获取商品详情页数据'],
            r"D:\WorkingSpace\wxSpace\main\wx-applet\applet-uis\yiwub-ui\static\apiJson\sub_info_api.json")
    reqBody(urls['获取商品类型数据'],
            r"D:\WorkingSpace\wxSpace\main\wx-applet\applet-uis\yiwub-ui\static\apiJson\sub_type_api.json")
