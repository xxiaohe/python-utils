import pandas as pd
import com.py.utils.FileUtils as fileUtils


def readExcel(path, sheet_name, usecols):
    excellSheet1 = pd.read_excel(path, sheet_name=sheet_name, usecols=usecols, names=None)
    return excellSheet1


def genSql(key, DESC):
    return "update fm_sys_lang_translation set `DESC`='%s' where `KEY`='%s';\n" % (DESC, key)


def transwords():
    excelpath = "D:\\temp\\DcitsFiles\\files1\\整理文档1714029105.xlsx"
    excellSheet2 = readExcel(excelpath, "前端", [1, 4])
    excellSheet3 = readExcel(excelpath, "表样标签", [2, 5])
    sqls = []
    for index, row in excellSheet2.iterrows():
        # if pd.isna(row.英文标签2):
        if pd.notna(row.英文标签2):
            print(row.英文标签2)
            sqls.append(genSql(row.中文标签, row.英文标签2))
    for index, row in excellSheet3.iterrows():
        if pd.notna(row.DESC2):
            print(row.DESC2)
            sqls.append(genSql(row.KEY, row.DESC2))
    fileUtils.writeOrignFile("D:\\temp\\DcitsFiles\\files1\\整理文档1714029105.sql", sqls)


if __name__ == "__main__":
    transwords()
