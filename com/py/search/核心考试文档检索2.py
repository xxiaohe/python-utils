import os  # 导入os模块，用于文件路径操作
from tkinter import Tk, filedialog  # 导入Tk和filedialog模块，用于图形界面操作
from docx import Document  # 导入Document模块，用于读取Word文档
import openpyxl


def search_in_docx(file_path, search_text):
    """
    在指定的Word文档中搜索指定的文本。
    :param file_path: Word文档的路径
    :param search_text: 要搜索的文本
    :return: 如果找到文本则返回True，否则返回False
    """
    try:
        listLine = []
        doc = Document(file_path)  # 打开Word文档
        lineNum = -1
        listLine.append(f"=====================  {file_path} ==================================")
        for para in doc.paragraphs:  # 遍历文档中的每个段落
            lineNum += 1
            if search_text.lower() in para.text.lower():  # 检查段落中是否包含搜索文本
                listLine.append(f"------------------------start------------------------------------")
                if lineNum < 8:
                    for i in range(lineNum - lineNum, lineNum + 8):
                        listLine.append(f"==> {doc.paragraphs[i].text} ")
                else:
                    for i in range(lineNum - 8, lineNum + 8):
                        listLine.append(f"==> {doc.paragraphs[i].text} ")
                listLine.append(f"-------------------------end-----------------------------------")
        for line in listLine:
            print(line)
    except Exception as e:
        print(f"读取文件 {file_path} 出错: {e}")


def search_in_directory(directory_path, search_text):
    """
    在指定目录及其子目录中的所有Word文档中搜索指定的文本。
    :param directory_path: 要搜索的目录路径
    :param search_text: 要搜索的文本
    """
    for root, dirs, files in os.walk(directory_path):  # 遍历目录
        for file in files:  # 检查每个文件
            if file.endswith('.docx'):  # 只处理Word文档（.docx格式）
                file_path = os.path.join(root, file)  # 获取文件的完整路径
                search_in_docx(file_path, search_text)
                # print(f"正在搜索文件 {os.path.basename(file_path)}...")  # 显示正在搜索的文件名
                # if search_in_docx(file_path, search_text):  # 调用search_in_docx函数
                # print(f"在文件 {os.path.basename(file_path)} 中找到了 '{search_text}'")  # 显示找到的文本
            if file.endswith('.txt'):
                listLine = []
                file_path = os.path.join(root, file)
                print(f"=====================  {file_path} ==================================")
                with open(file_path, 'r', encoding='utf-8') as file:
                    lineNum = -1
                    cxt = file.readlines()
                    for line in cxt:
                        lineNum += 1
                        if search_text.lower() in line.lower():
                            listLine.append(f"------------------------start------------------------------------")
                            if lineNum < 8:
                                for i in range(lineNum - lineNum, lineNum + 8):
                                    listLine.append(f"==> {cxt[i]} ")
                            else:
                                for i in range(lineNum - 8, lineNum + 8):
                                    listLine.append(f"==> {cxt[i]} ")
                            listLine.append(f"-------------------------end-----------------------------------")
                for line in listLine:
                    print(line)


def get_directory_path():
    """
    弹出图形界面对话框，让用户选择要搜索的目录。
    :return: 用户选择的目录路径
    """
    Tk().withdraw()  # 隐藏主窗口
    directory_path = filedialog.askdirectory(title="请选择要搜索的目录")  # 弹出选择文件夹的对话框
    return directory_path  # 返回用户选择的目录路径


def readExcel(target_field, excelPath):
    # 打开Excel文件
    workbook = openpyxl.load_workbook(excelPath)
    # 遍历所有工作表
    for sheetname in workbook.sheetnames:
        print(f"sheetname=> {sheetname}")
        worksheet = workbook[sheetname]
        # 遍历所有单元格
        printArr = []
        headerLine = []
        for row in worksheet.iter_rows():
            for cell in row:
                if cell.value is not None:
                    headerLine.append(cell.value)
                    # headerLine += (" | " + str(cell.value))
                else:
                    # headerLine += " | "
                    headerLine.append("")
            break
        printArr.append(headerLine)
        # print(f"header=> {headerLine}")

        for row in worksheet.iter_rows():
            findStr = False
            for cell in row:
                # 查询关键字段
                if type(cell.value) is str:
                    if target_field.lower() in str(cell.value.lower()):
                        findStr = True
                if type(cell.value) is int:
                    if target_field in str(cell.value):
                        findStr = True
            if findStr:
                cxtLine = []
                # lineStr = ""
                for cell in row:
                    if cell.value is not None:
                        cxtLine.append(cell.value)
                    else:
                        cxtLine.append("")
                printArr.append(cxtLine)
        for vitems in printArr:
            print(vitems)


if __name__ == "__main__":

    search_text = "机构现金超限"

    directory_path = r"D:\temp\考试"
    search_in_directory(directory_path, search_text)
    print((f"=====================  D:\\temp\\考试\\银行核心业务题库V1.2.xlsx =================================="))
    readExcel(search_text, r"D:\temp\考试\银行核心业务题库V1.2.xlsx")
