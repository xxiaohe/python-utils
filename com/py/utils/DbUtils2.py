from sqlalchemy import create_engine
from urllib.parse import quote_plus as urlquote
import pandas as pd
from com.py.utils.DbUtils import dbExec


class dbExec(dbExec):
    print(dbExec.port)
    DB_CONNECT = f'mysql+pymysql://{dbExec.userName}:{urlquote(dbExec.password)}@{dbExec.host}:{dbExec.port}/{dbExec.database}?charset=utf8'
    """
    描述：执行sql并将结果集转化为DataFrame对象
    """

    def execSqlToDataFrame(sql):
        engine = create_engine(dbExec.DB_CONNECT)
        # 使用pandas的read_sql_query函数查询数据库
        df = pd.read_sql_query('SELECT * FROM fm_sys_lang_translation', engine)
        return df


if __name__ == "__main__":
    data = dbExec.execSqlToDataFrame("select `KEY` from fm_sys_lang_translation")
    print(data)
