import pandas as pd

# 创建两个DataFrame用于生成不同的sheet页
df1 = pd.DataFrame({'Column1': [1, 2, 3], 'Column2': ['A', 'B', 'C']})
df2 = pd.DataFrame({'Column1': [4, 5, 6], 'Column2': ['D', 'E', 'F']})

# 使用ExcelWriter生成Excel文件
with pd.ExcelWriter('D:\\tmp\\output.xlsx') as writer:
    df1.to_excel(writer, sheet_name='Sheet1')
    df2.to_excel(writer, sheet_name='Sheet2')