import requests as req
import time
import json


def exec():
    url = r"https://www.58moto.com/conditions/%s-3-3-1-0-1-0-0-0-1-0-9-9-0-0-1-0-150-0-0-0-0-0.html"
    targets = []
    for i in range(1, 20):
        target = url % i
        time.sleep(5)
        targets.extend(get(target))
    print(json.dumps(targets))

def get(url):
    response = req.get(url)
    print("==get url=>"+url)
    target = response.text.split('__NEXT_DATA__')[1]
    s = target.find("{")
    e = target.rfind("}")
    print("length %s, index %s %s" % (len(target), s, e))
    target = target[s:e + 1]
    goods = json.loads(target)['props']['pageProps']['model']['goods']
    targets = []
    for good in goods:
        targets.append(info(good['goodId']))
    return targets


def info(goodId):
    url = r'https://www.58moto.com/good/%s.html' % goodId
    print("=info url=>"+url)
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36',
        'Cookie': 'Hm_lvt_a00967ead78a7ec427027805562ea5d3=1715233217,1715307156,1715580616; position=%7B%22cityCode%22%3A%22320500%22%2C%22cityName%22%3A%22%E8%8B%8F%E5%B7%9E%E5%B8%82%22%2C%22latitude%22%3A%2231.299379%22%2C%22longitude%22%3A%22120.619585%22%2C%22pinyin%22%3A%22SuZhouShi%22%2C%22provinceCode%22%3A%22320000%22%2C%22provinceName%22%3A%22%E6%B1%9F%E8%8B%8F%E7%9C%81%22%2C%22province%22%3A%22%E6%B1%9F%E8%8B%8F%E7%9C%81%22%7D; acw_tc=7670e80817155916881445904e2d3320b9403914c30bdcf9d0c197dd24; acw_sc__v2=6641da081ec013e1abb047cbf2c68b53f701f72f; tfstk=f9FE66jPa6CEFHwobcGrb2th4xhKwjIbK7iSrz4oRDmHOBtuQqgndDiSORozjlo3AXGIzluqPpAur7gualhneJiSA0lzyo71cs1bJyh-iisfGmAvy_h-q_GuS3VIPbjfc11bJyh-Z8_HBiCQSc3r-HD3qfvizc0ktumnIh0qkbmuqbbaSjFjhYWtovjKTxtmcogELmRSbQRPP2kEmymZWVFNwvo0-cRlczyHK0oTiG6aWoy3AVEPgGl0HlPrnb5eD2ygufmIi6AiOWU3nYVc2L4E6Dk0EA7htPlEYxViIE9tT8qLaveeyZUitlexDvXOXVPQGYoxQhj35Pon38rCXQmQ3PqZh5t6Gj2Yb7kmigkH2VmD47eeqLknWVof7NvfPIaxmrusyLp-pHuZcwEeeLHnWVof7NJJevCm7m_LY; ip=221.237.113.120; Hm_lpvt_a00967ead78a7ec427027805562ea5d3=1715591653; ssxmod_itna=eqjxcDgDui=DuDBPGKgDCu0m3KGQtTTrDB7x0vFeGzDAxn40iDtPZtrSexSOBizgmNPr0Qh0oemlmSR2TmYiSmqfM+DB3DEx0=PzAexiiuDCeDIDWeDiDG4GmB4GtDpxG=Djjtz1M6xYPDEjKDYxDrLaKDRxi7DDHd8x07DQy85gGqUgDDBpgvai0GKfp+noEC37BDqjKD9roDsOij8SAjnSGzKf3LxzbODlF0DCI1n65Hb4GdU2y1xUuevB2YSBxor7i33QDoYQhqQQ4LHmo4tBTeAMx4UsxxPPhtbBDDi1r9SAGDD=; ssxmod_itna2=eqjxcDgDui=DuDBPGKgDCu0m3KGQtTTrDBQDn9O9PDsatDLQDXRKT4L7dsok0SrKtQjQYqhQ4jWYk+0+oZxnyuGP8UkDT+uQtGnRwb1tGCdXFk=mibFq44+vkmzbqm/0FT+E0LRoxEiNecHt2im5tp7kzGm4I6eT43v4xzAb3BBh6lpQuWDdYxONiRma3gPp0CgWk98kQg8iUKkkueqYh1BeS4MfR=LfUzL6VbeD6QTR0GGxtBIQ61QqY=ZP2CrU2CYd5kn2YZwy0CQtG=zgnnZyjc4SBGiyt1QkvueZ0CtMfsllKPNSS2gIHZm5g37Duvx55R9v5F5gYay7uBDByQ5e5ygAArtHN0yKNA425YMKtKvM/7NDBoM7y/Y4LK4kO/txDKMOD7=DY9uGDoYD7K0i5GDD'}
    response = req.get(url, headers=headers)
    target = response.text.split('__NEXT_DATA__')[1]
    s = target.find("{")
    e = target.rfind("}")
    print("length %s, index %s %s" % (len(target), s, e))
    target = target[s:e + 1]
    good = json.loads(target)['props']['pageProps']['model']['good']['carList'][0]['carInfoList'][0]
    obj = {}
    obj['name'] = good['brandName'] + " " + good['goodName']
    obj['goodVolume'] = good['goodVolume']
    obj['maxPrice'] = good['maxPrice']
    obj['minPrice'] = good['minPrice']
    obj['goodCoolDown'] = good['goodCoolDown']
    obj['goodCylinder'] = good['goodCylinder']
    params = json.loads(target)['props']['pageProps']['model']['params']
    for item in params:
        if item['name'] == '品牌':
            obj['brandName'] = item['value']
        elif item['name'] == '车型':
            obj['vehicleModel'] = item['value']
        elif item['name'] == 'ABS':
            obj['ABS'] = item['value']
        elif item['name'] == '长x宽x高(mm)':
            obj['body'] = item['value']
        elif item['name'] == '油箱容量(L)':
            obj['tank'] = item['value']
        elif item['name'] == '最高车速(km/h)':
            obj['speed'] = item['value']
        elif item['name'] == '官方平均油耗(L/100km)':
            obj['oilConsumption'] = item['value']
        elif item['name'] == '续航里程(km)':
            obj['endurance'] = item['value']
    return obj


if __name__ == "__main__":
    exec()
    # get("")
