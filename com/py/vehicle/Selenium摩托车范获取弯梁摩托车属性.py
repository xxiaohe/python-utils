import pandas as pd
from scrapy import signals
from selenium import webdriver
from scrapy.http.response.html import HtmlResponse
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
import re
import json
import time
import ast
import pandas as pd
import com.py.utils.FileUtils as fileUtils

regex0 = '[\u4e00-\u9fa5|\-|\/]'


def getMobyle():
    chrome_driver_path = 'D:\\WorkingSpace\\pythonSpace\\bicenter_reptile\\demo\\chromedriver.exe'
    # 创建-个 Chrome 浏览器实例，并传入 ChromeDriver 的路径
    service = Service(executable_path=chrome_driver_path)
    # 启动Chrome浏览器
    web = webdriver.Chrome(service=service)
    web.maximize_window()
    # url = r"https://www.58moto.com/conditions/%s-3-3-1-0-1-0-0-0-1-0-9-9-0-0-1-0-150-0-0-0-0-0.html"
    url = r"https://www.58moto.com/conditions/%s-1-2-1-0-1-0-0-0-1-0-9-9-0-0-1-0-150-0-0-0-0-0.html"
    targets = []
    for i in range(1, 6):
        web.get(url % i)
        time.sleep(3)
        aList = web.find_elements(By.XPATH, '//*[@id="__next"]/div[3]/div/div/div[2]/div/div[12]/div/a')
        for aLable in aList:
            time.sleep(1)
            aLable.click()
            web.switch_to.window(web.window_handles[-1])
            time.sleep(3)
            name = web.find_element(By.XPATH, '//*[@id="__next"]/div[1]/div[3]/div[1]/div[1]/div/div[1]').text
            vehicleModel = web.find_element(By.XPATH,
                                            '//*[@id="__next"]/div[1]/div[3]/div[1]/table/tbody/tr[2]/td[4]').text
            trademark = web.find_element(By.XPATH,
                                         '//*[@id="__next"]/div[1]/div[3]/div[1]/table/tbody/tr[1]/td[4]').text
            referencePrice = web.find_element(By.XPATH,
                                              '//*[@id="__next"]/div[1]/div[3]/div[1]/table/tbody/tr[1]/td[2]').text
            size = web.find_element(By.XPATH, '//*[@id="__next"]/div[1]/div[3]/div[1]/table/tbody/tr[7]/td[4]').text
            fuelTank = web.find_element(By.XPATH, '//*[@id="__next"]/div[1]/div[3]/div[1]/table/tbody/tr[9]/td[2]').text
            maxSpeed = web.find_element(By.XPATH,
                                        '//*[@id="__next"]/div[1]/div[3]/div[1]/table/tbody/tr[10]/td[4]').text
            fuelConsumption = web.find_element(By.XPATH,
                                               '//*[@id="__next"]/div[1]/div[3]/div[1]/table/tbody/tr[11]/td[2]').text
            info = {
                'name': name,
                'vehicleModel': vehicleModel,
                'trademark': trademark,
                'referencePrice': referencePrice,
                'size': size,
                'fuelTank': fuelTank,
                'maxSpeed': maxSpeed,
                'fuelConsumption': fuelConsumption
            }
            print(info)
            fileUtils.writeOrignFile2(r"D:\temp\摩托车信息\弯梁摩托车信息(全).txt", 'a', [info])
            targets.append(info)
            web.close()
            web.switch_to.window(web.window_handles[0])
    time.sleep(5)
    web.close()
    return targets


def txtToExcel():
    path = r"D:\temp\摩托车信息\弯梁摩托车信息(全).txt"
    # 打开文件
    # 创建两个DataFrame用于生成不同的sheet页
    C_name = []
    C_vehicleModel = []
    C_trademark = []
    C_referencePrice = []
    C_size = []
    C_fuelTank = []
    C_maxSpeed = []
    C_fuelConsumption = []
    with open(path, 'r', encoding='utf-8') as file:
        # 按行读取
        for line in file:
            print(line)
            tar = ast.literal_eval(line.strip('\n'))
            C_name.append(tar['name'])
            C_vehicleModel.append(tar['vehicleModel'])
            C_trademark.append(tar['trademark'])
            C_referencePrice.append(
                0 if len(re.findall(regex0, tar['referencePrice'])) > 0 else float(tar['referencePrice']))
            C_size.append(tar['size'])
            C_fuelTank.append(0 if len(re.findall(regex0, tar['fuelTank'])) > 0 else float(tar['fuelTank']))
            C_maxSpeed.append(0 if len(re.findall(regex0, tar['maxSpeed'])) > 0 else float(tar['maxSpeed']))
            C_fuelConsumption.append(
                0 if len(re.findall(regex0, tar['fuelConsumption'])) > 0 else float(tar['fuelConsumption']))
    df1 = pd.DataFrame({'名称': C_name, '类型': C_vehicleModel, '品牌': C_trademark, '参考价格': C_referencePrice, '常长宽高': C_size,
                        '邮箱大小': C_fuelTank, '最高速度': C_maxSpeed, '百公里耗油': C_fuelConsumption})
    with pd.ExcelWriter(r'D:\temp\摩托车信息\弯梁摩托车信息(全)%s.xlsx' % int(time.time())) as writer:
        df1.to_excel(writer, sheet_name='弯梁摩托车')

if __name__ == "__main__":
    # getMobyle()
    txtToExcel()
