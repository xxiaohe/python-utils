import pymysql
import pandas as pd


class dbExec:
    host = '10.0.24.138'  # 主机名
    port = 3306  # 端口号，MySQL默认为3306
    userName = 'ENS_GL'  # 用户名
    password = '1q2w3e4R!@#$'  # 密码
    database = 'ens_gl'  # 数据库名称

    # 获取单列的所有值
    def execSql(sql):
        # 连接 MySQL 数据库
        dbStrs = set();
        try:
            conn = pymysql.connect(host=dbExec.host, port=dbExec.port, user=dbExec.userName, password=dbExec.password,
                                   database=dbExec.database)
            # 创建游标对象
            cursor = conn.cursor()
            cursor.execute(sql)
            # 遍历结果集
            for column1 in cursor:
                c1 = column1[0].replace('（', "(").replace('）', ")")
                dbStrs.add(c1.strip())
        finally:
            # 关闭游标和数据库连接
            cursor.close()
            conn.close()
        return dbStrs

    def execSqlToDataFrame(sql):
        try:
            conn = pymysql.connect(host=dbExec.host, port=dbExec.port, user=dbExec.userName, password=dbExec.password,
                                   database=dbExec.database)
            return pd.read_sql_query(sql, conn)
        finally:
            conn.close()
        return None


if __name__ == "__main__":
    data = dbExec.execSql("select `KEY` from fm_sys_lang_translation")
    print(data)
