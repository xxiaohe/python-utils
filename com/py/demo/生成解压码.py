import uuid
import json


def genCode(size):
    codes = set()
    while len(codes) < size:
        idCode = str(uuid.uuid4())
        codes.add(idCode[0:6].upper())
    return codes


if __name__ == "__main__":
    codes = genCode(200)
    idx = 8
    list = []
    for code in codes:
        list.append({
            "id": str(idx),
            "password": code
        })
        idx += 1
    print(json.dumps(list))
