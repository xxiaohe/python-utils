import pandas as pd
from scrapy import signals
from selenium import webdriver
from scrapy.http.response.html import HtmlResponse
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
import re
import requests
import time
import ast
import pandas as pd
import com.py.utils.FileUtils as fileUtils

loginPath = r"https://login.taobao.com/member/login.jhtml"
path = r"https://s.taobao.com/search?page=1&q=%E7%AB%A5%E8%A3%85&spm=a21bo.jianhua%2Fa.201867-main.1106.5af92a8959Nup5&tab=all"


def openTaoBao():
    chrome_driver_path = 'D:\\WorkingSpace\\pythonSpace\\bicenter_reptile\\demo\\chromedriver.exe'
    # 创建-个 Chrome 浏览器实例，并传入 ChromeDriver 的路径
    service = Service(executable_path=chrome_driver_path)
    # 启动Chrome浏览器
    web = webdriver.Chrome(service=service)
    web.maximize_window()
    web.get(loginPath)
    time.sleep(10)
    web.get(path)
    web.find_element(By.XPATH, '//*[@id="sortBarWrap"]/div[1]/div[1]/div/div[1]/div/div/div/ul/li[2]/div').click()
    time.sleep(2)
    web.find_element(By.XPATH, '//*[@id="sortBarWrap"]/div[2]/div/div/div/div').click()
    time.sleep(2)
    web.find_element(By.XPATH, '/html/body/div[5]/div/span/label[6]/span[2]/span').click()
    time.sleep(2)
    web.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(3)
    trList = web.find_elements(By.XPATH, '//*[@id="pageContent"]/div[1]/div[3]/div[3]/div/div')

    for ele in trList:
        try:
            time.sleep(1)
            name = ele.find_element(By.XPATH, 'a/div/div[1]/div[2]/div/span').text
            price = ele.find_element(By.XPATH, 'a/div/div[1]/div[4]/div[1]').text
            addr = ele.find_element(By.XPATH, 'a/div/div[3]/div[1]/a').text
            href = "https://" + ele.find_element(By.XPATH, 'a/div/div[3]/div[1]/a').get_attribute("href")
            img_src = ele.find_element(By.XPATH, 'a/div/div[1]/div[1]/img').get_attribute("src")
            img_name = img_src.split("/")[-1]
            img_rsp = requests.get(img_src)
            with open(r'C:\Users\Administrator\Desktop\temp\03\%s' % img_name, mode="wb") as f:
                f.write(img_rsp.content)
                f.flush()
                f.close()
        except Exception as e:
            name = None
            price = None
            addr = None
            href = None
            img_src = None
            img_rsp = None
            print(e)
        finally:
            print(name)
            print(price)
            print(addr)
            print(href)
            print(img_src)

    time.sleep(50)
    web.close()


if __name__ == "__main__":
    openTaoBao()
