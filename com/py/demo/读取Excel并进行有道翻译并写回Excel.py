import pandas as pd
from scrapy import signals
from selenium import webdriver
from scrapy.http.response.html import HtmlResponse
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
import requests
import json
import time
import re
import pandas as pd


def readExcel(path, sheet_name, usecols):
    excellSheet1 = pd.read_excel(path, sheet_name=sheet_name, usecols=usecols, names=None)
    return excellSheet1


def transwords():
    chrome_driver_path = 'D:\\WorkingSpace\\pythonSpace\\bicenter_reptile\\demo\\chromedriver.exe'
    # 创建-个 Chrome 浏览器实例，并传入 ChromeDriver 的路径
    service = Service(executable_path=chrome_driver_path)
    # 启动Chrome浏览器
    web = webdriver.Chrome(service=service)
    web.get("https://fanyi.youdao.com/#/")
    time.sleep(2)
    web.find_element(By.XPATH, '/html/body/div[5]/div/img[2]').click()
    web.find_element(By.XPATH, '/html/body/div[1]/div[1]/div/div[2]/div[2]/div/div[1]/div/div[1]/div[1]/div[1]').click()

    # excelpath="D:\\temp\\DcitsFiles\\files1\\翻译项整理(2).xlsx"
    excelpath = "D:\\temp\\DcitsFiles\\files1\\翻译项整理.xlsx"

    excellSheet1 = readExcel(excelpath, "后端", [0, 1, 2])
    excellSheet2 = readExcel(excelpath, "前端", [0, 1, 2])
    excellSheet3 = readExcel(excelpath, "表样标签", [2, 3])

    cum11 = []
    cum12 = []
    cum13 = []
    cum14 = []
    for index, row in excellSheet1.iterrows():
        try:
            web.find_element(By.XPATH, '//*[@id="js_fanyi_input"]').clear()
            web.find_element(By.XPATH, '//*[@id="js_fanyi_input"]').send_keys(row.英文标签)
            time.sleep(2)
            value = web.find_element(By.XPATH, '//*[@id="js_fanyi_output_resultOutput"]/p/span').text
            cum14.append(value)
        except:
            cum14.append("翻译失败")
        finally:
            cum11.append(row.key)
            cum12.append(row.中文标签)
            cum13.append(row.英文标签)
    print(cum14)
    cum21 = []
    cum22 = []
    cum23 = []
    cum24 = []
    for index, row in excellSheet2.iterrows():
        try:
            web.find_element(By.XPATH, '//*[@id="js_fanyi_input"]').clear()
            web.find_element(By.XPATH, '//*[@id="js_fanyi_input"]').send_keys(row.英文标签)
            time.sleep(3)
            value = web.find_element(By.XPATH, '//*[@id="js_fanyi_output_resultOutput"]/p/span').text
            cum24.append(value)
        except:
            cum24.append("翻译失败")
        finally:
            cum21.append(row.key)
            cum22.append(row.中文标签)
            cum23.append(row.英文标签)
    print(cum24)
    cum30 = []
    cum31 = []
    cum32 = []
    cum33 = []
    cum34 = []
    for index, row in excellSheet3.iterrows():
        try:
            web.find_element(By.XPATH, '//*[@id="js_fanyi_input"]').clear()
            web.find_element(By.XPATH, '//*[@id="js_fanyi_input"]').send_keys(row.DESC)
            time.sleep(2)
            value = web.find_element(By.XPATH, '//*[@id="js_fanyi_output_resultOutput"]/p/span').text
            cum34.append(value)
        except:
            cum34.append("翻译失败")
        finally:
            cum30.append("en-us")
            cum31.append("report")
            cum32.append(row.KEY)
            cum33.append(row.DESC)
    print(cum34)
    web.close()
    dfData1 = {
        "key": cum11,
        "中文标签": cum12,
        "英文标签": cum13,
        "中文标签2": cum14
    }
    dfData2 = {
        "key": cum21,
        "中文标签": cum22,
        "英文标签": cum23,
        "中文标签2": cum24
    }
    dfData3 = {
        "LANGUAGE": cum30,
        "ID": cum31,
        "KEY": cum32,
        "DESC": cum33,
        "KEY2": cum34,
    }
    df1 = pd.DataFrame(dfData1)
    df2 = pd.DataFrame(dfData2)
    df3 = pd.DataFrame(dfData3)
    with pd.ExcelWriter("D:\\temp\\DcitsFiles\\整理文档%s.xlsx" % int(time.time())) as writer:
        df1.to_excel(writer, sheet_name="后端", index=False)
        df2.to_excel(writer, sheet_name="前端", index=False)
        df3.to_excel(writer, sheet_name="表样标签", index=False)


if __name__ == "__main__":
    # excellSheet1 = readExcel("D:\\temp\\DcitsFiles\\files1\\翻译项整理.xlsx", "后端", [1, 2])
    #
    # for index, row in excellSheet1.iterrows():
    #     print(row.中文标签)
    transwords()
